#include <iostream>

using namespace std;

int main(){
//	 Declaración de arreglos de modo explicito
	int Arreglo1[10];
	
//	Declaración de arrglos de modo implicito
	int Arreglo2[] = {1,2,3,4,5,6,7,8,9,10};
	
	cout<<Arreglo2[0]<<endl; // Primer elemento
	
	int i;
	
	for (i=0; i<10;i++){
		cout<<"Indice "<<Arreglo2[i]<<endl;
	}
	
    return 0;
}

