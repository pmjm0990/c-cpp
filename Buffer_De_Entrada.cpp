#include <iostream>

using namespace std;

int main(){
	char Nombre[24];
	int Edad;
	
	cout<<"Ingresa tu edad"<<endl;
	cin>>Edad;
	cin.ignore(); // Elimina el primer caracter (\n) que se encuentre
//	cin.ignore(256,'\n'); -> Eliminara los 256 caracteres hasta que encuentre -> (\n)
	cout<<"Ingresa tu nombre"<<endl;
	cin.getline(Nombre,24);
	
	cout<<"Tu nombre es "<<Nombre<<" y tu edad es de "<<Edad<<endl;
	
    return 0;
}
