#include <iostream>

using namespace std;

int main(){
//	Asi se muestra una cadena de caracteres
	cout<<"Miguel Parra"<<endl;
//	Asi se muestra un caracter
	cout<<'M'<<endl;
		
	char Cadena1[] = "Miguel Parra";
	cout<<Cadena1<<endl;
	
	char Cadena2[] = {'M','i','g','u','e','l','\0'};//No olvidar colocar el (\0) al final
	cout<<Cadena2<<endl;
	
	
	char Cadena3[10];
	cout<<"Ingrese su nombre >"<<endl;
	cin.getline(Cadena3,23);/* la funcion getline, permite leer una cadena de caracteres junto con
	los espacios que conlleve el texto*/
		
	cout<<Cadena3<<endl; 
	
	
	return 0;
}
