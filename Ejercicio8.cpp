#include <iostream>

using namespace std;

int main(){
    /** Ejercicio 8 - Escriba un programa que calcule la cantidad
        de d�gitos que tiene un n�mero**/
    int numero,contador = 0;

    cout<<"Ingrese el numero:"<<endl;
    cin>>numero;

    do{
        numero = numero / 10;
        contador++;
    }while(numero!=0);

    cout<<"La cantidad de digitos es "<<contador<<endl;

    return 0;
}
