#include <iostream>

using namespace std;

int main(){
	int Numero;
	char Letra;
	
	cout<<"Ingrese su respuesta"<<endl;
	cin>>Numero;
	switch(Numero){
		case 1:
			cout<<"Esta dentro del caso 1"<<endl;
			break;
		case 2:
			cout<<"Esta dentro del caso 2"<<endl;
			break;
		case 3:
			cout<<"Esta dentro del caso 3"<<endl;
			break;
		case 4:
			cout<<"Esta dentro del caso 4"<<endl;
			break;
		default:
			cout<<"Su respuesta no esta dentro de las opciones"<<endl;
	}
	
	cout<<"Ingrese su respuesta"<<endl;
	cin>>Letra;
	switch(Letra){
		case 'L':
			cout<<"Esta dentro del caso 1"<<endl;
			break;
		case 'e':
			cout<<"Esta dentro del caso 2"<<endl;
			break;
		case 't':
			cout<<"Esta dentro del caso 3"<<endl;
			break;
		case 'r':
			cout<<"Esta dentro del caso 4"<<endl;
			break;
		default:
			cout<<"Su respuesta no esta dentro de las opciones"<<endl;
	}
	
	return 0;
}
