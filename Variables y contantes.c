#include <stdio.h>

/* Variables y constantes globales */

// Las variables y constantes se pueden usar de manera global o local en el codigo, con o sin modificadores.

int main() {
    /* Variables y constantes local */

    /* Variables normales */
    char Letra1;
    int Num1;
    float Real1;
    double Real2;

    /* Variables estaticas */

    static char Letra2;
    static int Num2;
    static float Real2;
    static double Real2;

    /* De Variables A Constantes */

    const char Letra3;
    const int NumEntero3;
    const float NumReal3;
    const double NumReal3;
    
    /* Variables utomaticas*/
    
	auto char Letra4;
    auto int NumEntero4;
    auto float NumReal4;
    auto double NumReal4;
    
	/* Variables Externas*/
	
    extern char Letra5;
    extern int NumEntero5;
    extern float NumReal5;
    extern double NumReal5;
	
	/* Variables registros*/
	 
	register char Letra6;
    register int NumEntero6;
    register float NumReal6;
    register double NumReal6;
    
    return 0;
}
